import Vuex from 'vuex'
import Store from '../store'

export default new Vuex.Store(Store)
