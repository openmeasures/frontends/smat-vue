import { createI18n } from 'vue-i18n'
import translations from '../translations'

export default createI18n({
  locale: 'en', // we pick this in a smarter way in LanguagePicker.vue
  fallbackLocale: 'en',
  messages: translations
})
