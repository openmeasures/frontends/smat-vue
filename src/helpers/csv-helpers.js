const CsvStringifierFactory1 = require('csv-writer/dist/lib/csv-stringifier-factory')
const csvStringifierFactory = new CsvStringifierFactory1.CsvStringifierFactory()

export function createArrayCsvStringifier (params) {
  return csvStringifierFactory.createArrayCsvStringifier(params)
}
