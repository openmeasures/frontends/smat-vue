const fs = require('fs')
const { join } = require('path')
const { map, get } = require('libnested')
const { isEqual } = require('lodash')
const { stringify } = require('json5')

const SRC_LOCALE = 'en'

const locales = fs.readdirSync(join(__dirname, '../src/translations'))
  .filter(locale => !locale.endsWith('.js'))

const fileNames = fs.readdirSync(join(__dirname, '../src/translations', SRC_LOCALE))

/* Generate src/translations/index.js */
updateIndex(locales, fileNames)

/* Update translation files */
fileNames.forEach(fileName => {
  const srcTranslation = loadFile(SRC_LOCALE, fileName)

  locales.forEach(locale => {
    if (locale === SRC_LOCALE) return

    // Load current translation
    const translation = loadFile(locale, fileName)
    var translationNeeded = []

    // Build a new translation based on the shape of the SRC_LOCALE translation
    // - this has the effect of dropping any translations no longer used
    const result = map(srcTranslation, (value, path) => {
      // Set the current locale translation for each "leaf" of translation object
      const localeTranslation = get(translation, path)

      if (localeTranslation) {
        // If the string is the same as that in SRC_LOCALE
        // add to list collection which might need translating
        if (localeTranslation === value) translationNeeded.push(path)
        return localeTranslation
      }

      // If the string is missing, insert SRC_LOCALE translation as default
      // - this isn't needed as translations fall back, but it makes it easier for translators
      translationNeeded.push(path)
      return value
    })

    logNeeds(locale, fileName, translationNeeded)
    if (isEqual(translation, result)) return

    const stringOutput = `module.exports = ${stringify(result, null, 2)}`
    fs.writeFile(join(__dirname, '../src/translations', locale, fileName), stringOutput, (err) => {
      if (err) console.log(err)
      else console.log(`✓ updated ${locale}/${fileName}`)
    })
  })
})

function loadFile (locale, fileName) {
  const path = join(__dirname, '../src/translations', locale, fileName)

  try {
    fs.accessSync(path)
    return require(path)
  } catch (err) {
    console.log(err)
    console.log(`✓ created ${locale}/${fileName}`)
    return {}
  }
}

function logNeeds (locale, fileName, translationNeeded) {
  if (translationNeeded.length) {
    console.log(`Translations needed ${locale}/${fileName}`)
    console.log(
      translationNeeded
        .map(path => '  ' + path.join('.'))
        .join('\n')
    )
    console.log('')
  }
}

function updateIndex (locales, fileNames) {
  // mix: The intention was help avoid gotchas by not having to maintain index.js files
  // in src/translation, unfortunately this code is now ugly ):
  // If this is hard to maintain, it's totally fine to scrap this and manually write src/translations/index.js
  let rootFile = 'module.exports = {\n'
  rootFile += locales.map(locale => {
    return [
      `  ${locale}: {`,
      `    ...require('./${locale}/base.js'),`,
      fileNames
        .filter(fileName => fileName !== 'base.js')
        .map(fileName => {
          return `    ${fileName.replace('.js', '')}: require('./${locale}/${fileName}')`
        })
        .join(',\n'),
      '  }'
    ].join('\n')
  }).join(',\n')
  rootFile += '\n}\n'
  // console.log(rootFile)

  fs.writeFileSync(join(__dirname, '../src/translations/index.js'), rootFile)
}
