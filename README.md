# smat-vue

This is a Vue interface for https://gitlab.com/smat-project/smat-be/-/blob/master/DOCS.md

SMAT is an [open source collective](https://opencollective.com/smat) that is fiscally hosted and not for profit dedicated to helping combat mis and disinformation as well as hate through simple to use visualization tools for journalists and activists.


## Development

Ensure you are using Node 18.X. Run `node --version` to check.
Later versions may work, but are likely to cause trouble. To easily change node versions you can use a tool such as [fnm](https://github.com/Schniz/fnm) or [nvm](https://github.com/nvm-sh/nvm). After you've set up node, run:

```bash
$ npm install
$ npm run serve
```

Compiles and hot-reloads for development.
Go the the address it tells you in the browser you'll see the app!

To change the API the app connects to, you can use the environment variable `SMAT_API`

e.g.

```bash
$ VUE_APP_API=http://127.0.0.1:5000 npm run serve
```

## Translations

see `src/translations/{locale}`

1. update `src/translations/en/*.js`
2. run `npm run i18n:update`

This will copy placeholders from the english translation over to all other locales,
and prune no longer used translations.
Finally, it will output info about which translations it thinks are missing.

## Deploy

Gitlab CI is set up (see `.gitlab-ci.yml`) to deploy code pushed to branches:
- `staging` => https://smat-project.gitlab.io/frontends/smat-vue
- `master` => https://www.smat-app.com


## Scripts

`npm run lint` - run the linter

`npm run build` - compiles and minifies for production

### How to

See [the wiki](https://gitlab.com/smat-project/frontends/smat-vue/-/wikis/Wiki-home) for some useful guides on how to develop this app

## NOTES

Useful links:

- vue configuration : https://cli.vuejs.org/config
- vue-cli: https://cli.vuejs.org/guide/
- vue forms : https://vuejs.org/v2/guide/forms.html
- vue-cli guide : https://appdividend.com/2018/02/09/vue-cli-tutorial-2018-example-scratch/
- social media meta tags
  - https://cards-dev.twitter.com/validator
  - https://developers.facebook.com/tools/debug/
  - https://css-tricks.com/essential-meta-tags-social-media/
