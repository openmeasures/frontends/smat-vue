const express = require('express')
const history = require('connect-history-api-fallback')
const serveStatic = require('serve-static')
const { join } = require('path')

const port = process.env.PORT || 8080

const app = express()
  .use(history())
  .use(serveStatic(join(__dirname, 'dist')))

app.listen(port, () => {
  console.log('Listening on port ' + port)
})
